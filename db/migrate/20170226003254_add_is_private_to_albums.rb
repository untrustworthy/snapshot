class AddIsPrivateToAlbums < ActiveRecord::Migration[5.0]
  def change
    add_column :albums, :is_private, :boolean
  end
end
