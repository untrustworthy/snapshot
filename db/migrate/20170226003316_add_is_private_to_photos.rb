class AddIsPrivateToPhotos < ActiveRecord::Migration[5.0]
  def change
    add_column :photos, :is_private, :boolean
  end
end
