class UsersController < ApplicationController
    
    before_action :authenticate_user!, only: [:index, :destroy]
    
    def index
      @users = User.all
    end
    
    def destroy
      @user = User.find(params[:id])
      @user.destroy
      redirect_to users_path
    end

end