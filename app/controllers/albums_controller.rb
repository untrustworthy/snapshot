class AlbumsController < ApplicationController
  
before_action :authenticate_user!, only: [:new, :edit, :delete]
  
  def index
    @albums = current_user.albums.all
  end
  
  def show
    @album = Album.find(params[:id])
  end
  
  def new
    @album = current_user.albums.build
  end

  def create
    @album = current_user.albums.build(album_params)
    if @album.save
      redirect_to @album
    else
      render :new
    end
  end
  
  def edit
    find_album
  end

  def update
    find_album

    if @album.update_attributes(album_params)
      redirect_to @album
    else
      render :edit
    end
  end
  
  def destroy
    find_album
    @album.photos.destroy
    @album.destroy
    redirect_to albums_path
  end
  
  private
  
  def find_album
     @album = Album.find(params[:id])
  end
  
  def album_params
    params.require(:album).permit(:title, :is_private)
  end
  
end