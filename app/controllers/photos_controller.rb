class PhotosController < ApplicationController

before_action :authenticate_user!, only: [:new, :edit, :delete]

 def index
   @photos = current_user.photos.all
 end
  
  def show
    find_photo
  end

  def new
    @photo = current_user.photos.new
  end

  def create
    @photo = current_user.photos.new(photo_params)
    if @photo.save
      redirect_to @photo
    else
      render :new
    end
  end

  def edit
    find_photo
  end

  def update
    
    find_photo

    if @photo.update_attributes(photo_params)
      redirect_to @photo
    else
      render :edit
    end
  end
  
  def destroy
    find_photo
    @photo.destroy
    redirect_to photos_path
  end

  private
  
  def find_photo
    @photo = Photo.find(params[:id])
  end
  
  def photo_params
    params.require(:photo).permit(:title, :file, :album_id, :is_private)
  end
  
  
end