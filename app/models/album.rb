class Album < ActiveRecord::Base
    has_many :photos, :dependent => :destroy
    belongs_to :user
    accepts_nested_attributes_for :photos
    validates :title, presence: true #Wymagany tytul albumu
    validates :is_private, inclusion: [true, false]
end
