class Photo < ActiveRecord::Base
  
  #Wymagany tytul zdjecia
  validates :title, :file, presence: true 
  
  #Zalacznik i walidacja zalacznika
  has_attached_file :file, styles: {large: "1000x1000>", medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment :file, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }, size: { in: 0..5.megabytes }
  validates :is_private, inclusion: [true, false]
  
  #Asocjacje
  belongs_to :album
  belongs_to :user
end