require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  
  test "should show link to sign up" do
    get 'index'
    assert_select 'a', attributes: {:href => 'users/sign_up'}
    assert_response :success
  end
end
