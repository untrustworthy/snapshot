require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
 include Devise::Test::IntegrationHelpers
 
  test "show user form" do
    get '/users/sign_in'
    assert_response :success
    
    assert_select 'form' 
    assert_response :success
  end
  
  
  test "show index" do
    get '/users'
    assert_redirected_to new_user_session_path
  end
  
end
