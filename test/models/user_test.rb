require 'test_helper'

class UserTest < ActiveSupport::TestCase
   include Devise::Test::IntegrationHelpers
   
  test "cant login" do
    assert_not users(:two).valid?
  end

end
